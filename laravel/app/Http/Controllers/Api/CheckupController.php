<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CheckupsCollection;
use App\Models\Checkup;
use App\Models\Patient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Request as Api;
use Symfony\Component\HttpFoundation\Response;

class CheckupController extends Controller
{

 private $model;

 public function  __construct() {
  $this->checkups = new Checkup;
  $this->patients = new Patient;
}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $search = Api::get('search');
        if($search != null) {
           $model = $this->checkups::whereHas('patient', function($q) use ($search) {
            $q->where('name', 'LIKE', '%' . $search . '%')->
            orWhere('id', 'LIKE', '%' . $search . '%');
        })->orWhere('id', 'LIKE', '%' . $search . '%')
           ->orderBy('id','desc')->paginate(10);

       } else {
          $model =$this->checkups->orderBy('id','desc')->paginate(10);
      }
      $modelResource = CheckupsCollection::collection($model);
      $response = [
          'pagination' => [
            'total' => $model->total(),
            'current_page' =>  $model->currentPage(),
            "page_url" => "patients?page=",
            'per_page' => $model->perPage()
        ],
        'data' =>  $modelResource
    ];
    return response()->json($response, Response::HTTP_OK);
}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $model =  $this->checkups::find($id);
      DB::beginTransaction();
      try {
       $model->destroy($id);
       DB::commit();
       return response()->json(['success'=>'Data removed successfully!'], 200);
   } catch (\Exception $e) {
       DB::rollback();
       return response()->json(['message' => $e->getMessage()], 500);
   }
}

}
