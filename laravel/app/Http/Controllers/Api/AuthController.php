<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;
use Symfony\Component\HttpFoundation\Response;
use cors;

class AuthController extends Controller
{

  public function login()
  {
    try {
      if(Auth::attempt(['email' => request('email'), 'password' => request('password')])) { 
        $user = Auth::user();
        if($user->status != 1) {
          abort(500, 'The account is deactivated by the admin.');
        }
        $success['token'] =  $user->createToken('MyApp')->accessToken; 
        $success['name'] =  $user->name;
        $success['role'] =  $user->getRoleNames()->first();
        $success['email'] =  $user->email;
        $success['permissions'] = $user->getAllPermissions()->pluck('name');
        return response()->json(['success' => $success], Response::HTTP_OK);
      } 
      else{ 
        return response()->json(['message'=>'Incorrect credentials, Please try again.'], Response::HTTP_UNAUTHORIZED); 

      } 
    } catch (\Exception $e) {
      return response(['message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
    }
  }
/** 
     * REGISTER API 
     * 
     * @return \Illuminate\Http\Response 
     */ 
public function register(Request $request) 
{ 
  $validator = Validator::make($request->all(), [ 
    'name' => 'required', 
    'email' => 'required|email|unique:users', 
    'password' => 'required', 'string', 'min:8',
  ]);
  if ($validator->fails()) { 
    return response()->json(['message'=>$validator->errors()],  Response::HTTP_UNAUTHORIZED);    
  }
  DB::beginTransaction();
  try {
    $input = $request->all(); 
    if(!Role::whereName('sub-admin')->exists()) {
     $role = Role::create(['name' => 'sub-admin']);
   }
   
   $input['password'] = bcrypt($input['password']); 
   $user = User::create($input); 
       /*
       *  ASSIGN ROLE
       */
       $user->assignRole('sub-admin');
       // foreach ((Array)$request->permissions as $permission) {
       //   $user->givePermissionTo($permission);
       // }
       $user->givePermissionTo(['view' , 'create']);
       $success['token'] =  $user->createToken('MyApp')->accessToken; 
       $success['name'] =  $user->name;
       $success['role'] =  $user->getRoleNames()->first();
       $success['email'] =  $user->email;
       $success['permissions'] = $user->getAllPermissions()->pluck('name');
       DB::commit(); 
       return response()->json(['success'=> $success], Response::HTTP_CREATED); 
     } catch (Exception $e) {
      DB::rollback();
      return response()->json(['message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
    }
  }


}
