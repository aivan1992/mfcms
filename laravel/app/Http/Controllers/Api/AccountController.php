<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\AccountResource;
use App\Http\Resources\AccountsCollection;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request as Api;
use Spatie\Permission\Models\Role;
use Symfony\Component\HttpFoundation\Response;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $model; 
    
    public function  __construct() {
      $this->model = new User;    
    }  


    public function index()
    {
      $search = Api::get('search');
      if($search != null) {
       $model = $this->model::
       where('name', 'LIKE', '%' . $search . '%')
       ->orWhere('email', 'LIKE', '%' . $search . '%')
       ->orWhere('id', 'LIKE', '%' . $search . '%')
       ->orderBy('id','desc')->paginate(10);
     } else {
      $model =$this->model->orderBy('id','desc')->paginate(10);
    }
    $modelResource = AccountsCollection::collection($model);
    $response = [
      'pagination' => [
        'total' => $model->total(),
        'current_page' =>  $model->currentPage(),
        "page_url" => "accounts?page=",
        'per_page' => $model->perPage()
      ],
      'data' =>  $modelResource
    ];
    return response()->json($response, Response::HTTP_OK);
  }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      // return view('pages.accounts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
     DB::beginTransaction();
     try {
       $request->validate([
        'name' => "required|string|max:255",
        'email' => "required|string|email|max:255|unique:users",
      ]);
       $user =  $this->model::create([
         'name' => $request->name,
         'email' => $request->email,
         'password' => Hash::make($request->password),  
       ]);
        /*
        *  CREATE ROLE IF NOT EXIST
        */ 
        if(!Role::whereName($request->role)->exists()) {
         $role = Role::create(['name' => $request->role]);
       }
       /*
       *  ASSIGN ROLE
       */
       $user->assignRole($request->role);
       if($request->role == 'super-admin') {
         $user->givePermissionTo(['view', 'create', 'edit' , 'remove']);  
         } else {

        foreach ((Array)$request->permissions as $key => $value) {
          if($value === true) {
           $user->givePermissionTo($key);
         }
       }
     }
     DB::commit();
     return response()->json(['success'=>'Data saved successfully'], Response::HTTP_CREATED); 

   } catch (Exception $e) {
    DB::rollback();
    return response()->json(['message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
  }
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     $model = new AccountResource($this->model::find($id));
     return response()->json($model, Response::HTTP_OK); 
   }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
     $user =  $this->model::find($id);
     $request->validate([
      'name' => "required|string|max:255",
      'email' => "required|string|email|max:255",
    ]);

     /*
      * DECLARE ARRAY VARIABLE
      */
     $details_array = array(
      'name' => $request->name,
      'email' => $request->email
    );
        /*
        *
        */
      /*
      * CHECK IF PASSWORD HAS INPUT
      */
      if($request->password != '') {
       $request->validate([
         'password' => "string|min:8",
       ]);   
       $password = array('password' => Hash::make($request->password));
       // PUSH PASSWORD INPUT TO ARRAY
       $merged_array = array_merge($details_array, $password);
     }
      /*
      * 
      */
      DB::beginTransaction();
      try {
      /*
      * UPDATE RECORDS
      */
      $user->update($merged_array ?? $details_array);
      /*
      * 
      */

        /*
        *  CREATE ROLE IF NOT EXIST
        */ 
        if(!Role::whereName($request->role)->exists()) {
         $role = Role::create(['name' => $request->role]);
       }
      /*
       */
      /*
       *  UPDATE ROLE
       */
       $user->syncRoles($request->role);
      /*
       *
       */
       /*
       *  UPDATE PERMISSIONS
       */
       if($request->role == 'super-admin') {
         $user->givePermissionTo(['view', 'create', 'edit' , 'remove']);
       } else {
         foreach ((Array)$request->permissions as $key => $value) {
          $value ? $user->givePermissionTo($key) : $user->revokePermissionTo($key);
       }
     }
      /*
       *
       */
      DB::commit();
      return response()->json(['success'=>'Data updated successfully'], Response::HTTP_OK); 
    } catch (Exception $e) {
      DB::rollback();
      return response()->json(['message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
    }
  }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $model =  $this->model::find($id);
      DB::beginTransaction();
      try {
       $model->destroy($id);
       DB::commit();
       return response()->json(['success'=>'Data removed successfully!'], 200);   
     } catch (\Exception $e) {
       DB::rollback();
       return response()->json(['message' => $e->getMessage()], 500);
     }
   }

   public function updateStatus(Request $request)
   {
    try {
      $user = User::findOrFail($request->id);
      $user->update(['status' => $request->status]);
       return response()->json(true, Response::HTTP_OK); 
    } catch(\Exception $e) {
       return response()->json(['message' => $e->getMessage()], 500);
    }
   }


 }
