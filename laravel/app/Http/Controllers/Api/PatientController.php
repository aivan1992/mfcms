<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\PatientsCollection;
use App\Http\Resources\PatientsRecordsRecovery;
use App\Models\Patient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Request as Api;

class PatientController extends Controller
{

  private $model;

  public function  __construct() {
    $this->model = new Patient;
  }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $search = Api::get('search');

      if($search != null) {

       $model = $this->model::
       where('name', 'LIKE', '%' . $search . '%')
       ->orWhere('id', 'LIKE', '%' . $search . '%')
       ->orderBy('id','desc')->paginate(10);

     } else {

      $model =$this->model->orderBy('id','desc')->paginate(10);

    }

    $modelResource = PatientsCollection::collection($model);

    $response = [
      'pagination' => [
        'total' => $model->total(),
        'current_page' =>  $model->currentPage(),
        "page_url" => "patients?page=",
        'per_page' => $model->perPage()
      ],
      'data' =>  $modelResource
    ];

    return response()->json($response, Response::HTTP_OK);

  }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

     DB::beginTransaction();

     try {

      $request->validate([
        'name' => "required|string|max:255",
        'date_of_birth' => "required"
      ]);

      $this->model::create($request->all());


      DB::commit();

      return response()->json(['success'=>'Data saved successfully'], Response::HTTP_CREATED);

    } catch (Exception $e) {

      DB::rollback();

      return response()->json(['message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);

    }

  }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
     $data = $this->model::find($id);
     return response()->json($data, Response::HTTP_OK);
   }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

      $model =  $this->model::find($id);

      DB::beginTransaction();

      try {

        $model->checkups()->delete();

        $model->destroy($id);

        DB::commit();

        return response()->json(['success'=>'Data removed success successfully!'], 200);

      } catch (\Exception $e) {

       DB::rollback();

       return response()->json(['message' => $e->getMessage()], 500);

     }

   }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function patientsRecordsRecovery()
    {


     $search = Api::get('search');

     if($search != null) {

       $model = $this->model::
       where('name', 'LIKE', '%' . $search . '%')
       ->orWhere('id', 'LIKE', '%' . $search . '%')
       ->orderBy('id','desc')->paginate(5);

     } else {

      $model = $this->model->orderBy('id','desc')->paginate(5);

    }

    $modelResource = PatientsRecordsRecovery::collection($model);

    $response = [
      'pagination' => [
        'total' => $model->total(),
        'current_page' =>  $model->currentPage(),
        "page_url" => "patients-records-recovery?page=",
        'per_page' => $model->perPage()
      ],
      'data' =>  $modelResource
    ];

    return response()->json($response, Response::HTTP_OK);
  }

  public function checkExist(Request $request) {

   try {

     $po = $this->model::where($request->all())->get();

     $count = count($po);

     $message = '';

     if($count > 0) {

       $message = ['true' => [
        'message' => 'The name and date-of-birth exist on the system, would you like to continue? The record will be added as a new patient.'
      ]];

    } else {

      $message = 'false';

    }

    return response()->json(['response' => $message], Response::HTTP_OK);

  } catch(Exception $e) {

    return response()->json(['response' => $e->getMessage()], Response::HTTP_BAD_REQUEST);

  }
}

}
