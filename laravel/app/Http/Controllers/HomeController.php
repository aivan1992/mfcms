<?php

namespace App\Http\Controllers;

use App\Exceptions\PageNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    

return 'test';

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function page($page) 
    {


        // if($this->checkRoute('pages/' . $page)) {
        if($page == 'home') {

            return view('home');
        }

        try {

            return view('pages/' . $page);

        } catch (\Exception $e) {

            throw new PageNotFoundException();

        }

            

        // } 

        // abort(404);      throw new PageNotFoundException();

    }

    // public function checkRoute($route) {
    //     $routes = \Route::getRoutes()->getRoutes();
    //     foreach($routes as $r){
    //         if($r->getUri() == $route){
    //             return true;
    //         }
    //     }

    //     return false;
    // }

}
