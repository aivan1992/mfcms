<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class CheckupsCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    // 'patient_id','date_of_check_up', 'treatment', 'findings', 'complaints', 'weight', 'temperature', 'height'



    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'patient_id' => $this->patient_id,
            'date_of_check_up' => $this->date_of_check_up,
            'treatment' => $this->treatment,
            'findings' => $this->findings,
            'complaints' => $this->complaints,
            'weight' => $this->weight,
            'temperature' => $this->temperature,
            'height' => $this->height,
            'patient_name' => $this->patientName
        ];  
    }
}
