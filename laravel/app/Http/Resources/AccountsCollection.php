<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class AccountsCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'email' => $this->email,
            'role' => $this->getRoleNames()->first(),
            'id' => $this->id,
            'permissions' => $this->getAllPermissions()->pluck('name'),
            'status' => $this->status
        ];
    }
}
