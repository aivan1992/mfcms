<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PatientResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        
         /* 

       'age','name','date_of_birth','resident_address','business_address','sex','civil_status','occupation','tel_no','mobile_no','slug'

       */

       return [
        'age' => $this->age,
        'resident_address' => $this->resident_address,
        'business_address' => $this->business_address,
        'occupation' => $this->occupation,
        'tel_no' => $this->tel_no,
        'id' => $this->id,
        'name' => $this->name,
        'date_of_birth' => $this->date_of_birth,
        'mobile_no' => $this->mobile_no,
        'sex' => $this->sex,
        'civil_status' => $this->civil_status
    ];
        
    }
}
