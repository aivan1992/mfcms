<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class PatientsRecordsRecovery extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'date_of_birth' => $this->date_of_birth,
            'sex' => $this->sex,
            'deleted_at' => 'Dec. 11, 2019',
        ];
    }
}
