<?php

namespace App\Models;

use App\Models\Checkup;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Patient extends Model
{

  use Sluggable, SoftDeletes;

  protected $fillable = [
    'age','name','date_of_birth','resident_address','business_address','sex','civil_status','occupation','tel_no','mobile_no','slug'
  ];


  public function sluggable()
  {
    return [
      'slug' => [
        'source' => 'name'
      ]
    ];
  }

  public function setNameAttribute($value)
  {
    $this->attributes['name'] =  Str::title($value);
  }
  public function getDateOfBirthAttribute($value)
  {
   return (new Carbon($value))->format('m/d/Y');
  }

  public function setResidentAddressAttribute($value)
  {
    $this->attributes['resident_address'] =  ucwords($value);
  }

   public function setBusinessAddressAttribute($value)
  {
    $this->attributes['business_address'] =  ucwords($value);
  }

  public function setCivilStatusAttribute($value)
  {
    $this->attributes['civil_status'] = ucwords($value);
  }

  public function checkups()
  {
    return $this->hasMany(Checkup::class, 'patient_id');
  }

}
