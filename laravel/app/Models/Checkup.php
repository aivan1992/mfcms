<?php

namespace App\Models;

use App\Models\Patient;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Checkup extends Model
{

	use SoftDeletes;

	protected $fillable = [
		'patient_id','date_of_check_up', 'treatment', 'findings', 'complaints', 'weight', 'temperature', 'height'
	];


  public function patient()
  {
    return $this->belongsTo(Patient::class, 'patient_id');
  }

  public function getPatientNameAttribute() 
  {
    return $this->patient()->value('name');
  }


  public function setTreatmentAttribute($value)
  {
    $this->attributes['treatment'] = ucwords($value);
  }

  public function setFindingsAttribute($value)
  {
    $this->attributes['findings'] = ucwords($value);
  }

  public function setComplaintsAttribute($value)
  {
    $this->attributes['complaints'] = ucwords($value);
  }

  public function getDateOfCheckUpAttribute( $value )
  {
    return (new Carbon($value))->format('m/d/Y');
  }

}
