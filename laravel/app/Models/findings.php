<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class findings extends Model
{
    protected $table = 'findings';
	protected $fillable = ['name'];
}

