<?php

use App\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();


    	$permissions = ['edit', 'remove', 'create', 'view'];


    	foreach ($permissions as $permission) {

    		if(!Permission::whereName($permission)->exists()) {
    			Permission::create(['name' => $permission]);
    		}

    	}


    	if(!Role::whereName('super-admin')->exists()) {
    		$role = Role::create(['guard_name' => 'web', 'name' => 'super-admin']);
    		$role->givePermissionTo(Permission::pluck('name'));

    	} else {

    		$role = Role::findByName('super-admin');
    		$role->givePermissionTo(Permission::pluck('name'));
    	}


    	if(!Role::whereName('sub-admin')->exists()) {
    		$role = Role::create(['guard_name' => 'web','name' => 'sub-admin']);
    		// $role->givePermissionTo(['edit', 'create', 'view']);
    	} else {

    		$role = Role::findByName('sub-admin');
    		// $role->givePermissionTo(['create', 'view', 'edit']);
    	}



        if(!User::whereEmail('admin@gmail.com')->first()) {

       $user = User::create([
        'name' => 'admin',
        'password' => bcrypt('admin'),
        'email' => 'admin@gmail.com'
    ]);
       $user->assignRole('super-admin');
       $user->givePermissionTo(['view' , 'create', 'edit', 'remove']);
        
        }



   }
}
