<?php

use App\Models\Patient;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class PatientsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	// 'age','name','date_of_birth','resident_address','business_address','sex','civil_status','occupation','tel_no','mobile_no'

    	$faker = Faker::create();

    	$permissions = ['view', 'remove', 'create', 'edit'];

    	$date_of_birth = ['2000-04-11', '2001-04-21', '1995-04-11', '1995-07-19'];

    	foreach (range(1,250) as $index) {

            $gender = Arr::random(['male', 'female']);

    		$user = Patient::create([
    			'name' => $faker->name($gender),
    			'age' => rand(18, 40),
    			'date_of_birth' => Arr::random($date_of_birth),
    			'resident_address' => $faker->address,
    			'business_address' => $faker->address,
    			'sex' => $gender,
    			'civil_status' => Arr::random(['married', 'single']),
    			'occupation' => $faker->jobTitle,
    			'tel_no' => $faker->e164PhoneNumber,
    			'mobile_no' => $faker->phoneNumber,

    		]);

    		

    	};
    }
}
