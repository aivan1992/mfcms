<?php

use App\User;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class AccountsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker::create();

    	$permissions = ['view', 'remove', 'create', 'edit'];

    	foreach (range(1,150) as $index) {

    		$user = User::create([
    			'name' => $faker->name,
    			'email' => $faker->unique()->safeEmail,
    			'password' => bcrypt(Str::random(20)),		
    		]);

    		$user->assignRole('sub-admin');

    		foreach($permissions as $permission) {
    			$user->givePermissionTo($permission);
    		}

    	};
    }
}
