<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\Models\Rental;
use Faker\Generator as Faker;

$factory->define(Rental::class, function (Faker $faker) {
   return [
		'customer_id' => 1 ,
		'status' => rand(1, 4),
		'transaction_id' => rand(0, 1), 
		'product_id' => rand(0, 1), 
		'requested_days' => rand(0, 5),
		'approved_days' => rand(0, 5),
		'date_start' => '2019-11-11',
		'date_end' => '2019-11-14',
		'penalty_days' => 4,
		'total_rent_amount' => 2000,
		'penalty_amount' => 50,
		'payment_status' => 1
	];
});
