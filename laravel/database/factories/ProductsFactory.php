<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Product;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Product::class, function (Faker $faker) {
	return [
		'title' => $faker->name,
		'serial' => Str::random(10),
		'daily_rate' => rand(0, 5000),
		'category_id' => rand(1, 4),
		'status' =>  rand(0, 1),
		'notes' =>  Str::random(10)
	];
});
