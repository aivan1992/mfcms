<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/




Route::group(['namespace' => 'Api'], function () {

	Route::post('login', 'AuthController@login');

	Route::post('register', 'AuthController@register');
	
	Route::group(['middleware' => 'auth:api'], function () {

		Route::resource('patients', 'PatientController');
		Route::get('patients-records-recovery', 'PatientController@patientsRecordsRecovery');
		Route::post('patients/check-exist', 'PatientController@checkExist');

		Route::post('accounts/update-status', 'AccountController@updateStatus');
		Route::resource('accounts', 'AccountController');

		Route::resource('checkups/patient-checkup-records', 'CheckupPatientCheckupRecordsController');
		Route::resource('checkups', 'CheckupController');

	});

});


