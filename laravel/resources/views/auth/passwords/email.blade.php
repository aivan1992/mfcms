@extends('layouts.user-view')

@section('content')

 <div class="login-register" style="background-image:url({{asset('hrms/assets/images/background/login-register.jpg')}});">        
            <div class="login-box card">
            <div class="card-body">
                   @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                 <form method="POST" action="{{ route('password.email') }}">
                        @csrf
                    <h3 class="box-title m-b-20">Recover Password</h3>
                    <div class="form-group">
                      <div class="col-xs-12">
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="E-Mail Address">

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                      </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                      <div class="col-xs-12">
                        <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Reset</button>
                      </div>
                    </div>
                  </form>
            </div>
          </div>
        </div>

@endsection
