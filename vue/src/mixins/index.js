export const mixin = {
  data() {
    return {};
  },

  methods: {
    toTitleCase(str) {
      return str.replace(/\w\S*/g, function(txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
      });
    },

    removeNotNumber(val) {
      return (val = val.toString().replace(/[\$\,a-zA-Z\s]/g, ""));
    },

    formatAsThousands(val) {
      val = (+val).toFixed(2);

      return (val = val.replace(/\B(?=(\d{3})+(?!\d))/g, ","));
    },

    toast(body, variant = "success") {
      return new Promise((resolve, reject) => {
        this.$bvToast.toast(body, {
          title: ``,
          toaster: "b-toaster-bottom-right",
          solid: true,
          appendToast: true,
          variant: variant,
        });

        setTimeout(() => {
          resolve(true);
        }, 2000);
      });
    },

    swal(text, type = "success") {
      return new Promise((resolve, reject) => {
        Swal.fire({
          title: "Are you sure?",
          text: text,
          icon: "warning",
          confirmButtonText: "Proceed",
          showCancelButton: true,
          confirmButtonColor: "gray",
        }).then((result) => {
          if (result.value) {
            resolve(true);
          } else {
            resolve(false);
          }
        });
      });
    },

    swalRemove(text, type = "success") {
      return new Promise((resolve, reject) => {
        Swal.fire({
          title: "Are you sure?",
          text: "You wont",
          icon: "error",
          confirmButtonText: "Proceed",
          showCancelButton: true,
          confirmButtonColor: "gray",
        }).then((result) => {
          if (result.value) {
            resolve(true);
          } else {
            resolve(false);
          }
        });
      });
    },

    swalSuccessMessage(title, text) {
      Swal.fire({
        icon: "success",
        title: title || "The data has been saved",
        text: text,
        showConfirmButton: false,
        timer: 3000,
      });
    },

    swalErrorMessage(title, text) {
      vm.ErrorAudio();
      Swal.fire({
        icon: "error",
        title: title || "Error occured",
        text: text || "",
        showConfirmButton: false,
        timer: 3000,
      });
    },
  },
};
