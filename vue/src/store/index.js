import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

import "./axiosInterceptors";

import auth from './modules/auth'
import accounts from './modules/accounts'
import patients from './modules/patients'
import checkups from './modules/checkups'


const store = new Vuex.Store({
	modules: {
		auth,
		accounts,
		patients,
		checkups
	},
	state: {},
	mutations: {},
	actions: {}

})


export default store;