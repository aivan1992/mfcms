import actions from './actions';
import getters from './getters';
import mutations from './mutations';


const state = {

	patients: null,
	pagination: null,

	patient: {
		permissions: {
			read: false,
			create: false,
			edit: false,
			remove: false
		},
		name: '',
		email: '',
		password: '',
		role: ''
	},

	patients_records_recovery: null,
	patients_records_recovery_pagination: null,
	
};

export default {
	namespaced: true,
	state,
	actions,
	getters,
	mutations,
};
