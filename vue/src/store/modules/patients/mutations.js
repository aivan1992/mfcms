

const setPatients = (state, payload) => {
	state.patients = payload.data
	state.pagination = payload.pagination
}

const setPatient = (state, payload) => {
	// state.account.name = payload.data.name
	// state.account.email = payload.data.email
	// state.account.role = payload.data.role

	// state.account.permissions = {
	// 	read: payload.data.permissions.read,
	// 	create: payload.data.permissions.create,
	// 	edit: payload.data.permissions.edit,
	// 	remove: payload.data.permissions.remove
	// }
}

const setPatientsRecordsRecovery = (state, payload) => {
    state.patients_records_recovery = payload.data
	state.patients_records_recovery_pagination = payload.pagination
}


export default {
	setPatients,
	setPatient,
	setPatientsRecordsRecovery
};
