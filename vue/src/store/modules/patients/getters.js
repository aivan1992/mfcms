
const getPatients = (state) => {
	return state.patients;
}

const getPagination = (state) => {
	return state.pagination;
}

const getPatient = (state) => {
	return state.patient;
}

const getPatientsRecordsRecovery = (state) => {
	return state.patients_records_recovery;
}

const getPatientsRecordsRecoveryPagination = (state) => {
	return state.patients_records_recovery_pagination;
}

export default {
	getPatients,
	getPagination,
	getPatient,
	getPatientsRecordsRecovery,
	getPatientsRecordsRecoveryPagination
};
