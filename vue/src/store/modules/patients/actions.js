import Axios from "axios";

import {default_header, multipart_header } from '@/request_headers.js'

const getPatients = ({commit},payload)=> {

    /**
      *  @params payload.data, payload.nextPrev
      *
      *  Set empty string nextPrev if undefined otherwise append the nextPrev to url
      *
      */
      let nextPrev = payload.nextPrev != undefined ? payload.nextPrev : '';
      /**/

    /**
      * Convert JSON to URL query string
      *
      */
      var queryString = Object.keys(payload.data).map((key) => {
        return encodeURIComponent(key) + '=' + encodeURIComponent(payload.data[key])
      }).join('&');
      /**/


      return new Promise((resolve,reject)=> {
        Axios.get(`patients?${queryString} ${nextPrev}`, default_header).then((res)=>{
         if (res.status == 200) {
          commit('setPatients', res.data);
          resolve(true);
        }
      }).catch(err=>{
        reject(err.response.data.errors || err.response.data.message)
      })
    })
    }

    const editPatient = ({commit},payload)=>{
      return new Promise((resolve,reject)=>{
        Axios.get('patients/edit' + {id:payload}, default_header).then((res)=>{

         if (res.status == 200) {
          commit('setPatients', res.data);
          resolve(true);
        }

      }).catch(err=>{
        reject(err.response.data.errors || err.response.data.message)
      })
    })
    }

    const createPatient = (state, payload) => {
      return new Promise((resolve, reject) => {
        Axios.post("patients", payload, default_header)
        .then(response => {
          if (response.status == 201) {
            resolve(response.data);
          }
        })
        .catch(err => {
          reject(err.response.data.errors || err.response.data.message);
        });
      });
    };

    const deletePatient = ({commit},payload)=>{

      return new Promise((resolve,reject)=>{
        Axios.delete('patients/'+ payload.id, default_header).then((res)=>{
          resolve(res.data)
        }).catch(err=>{
          reject(err.response.data.errors || err.response.data.message)
        })
      })
    }

    const updatePatient = ({commit},payload)=>{
      return new Promise ((resolve,rej)=>{
        Axios.put('patients',payload, default_header).then((res)=>{
          resolve(true)
        }).catch(err=>{
          reject(err.response.data.errors || err.response.data.message)
        })
      })
    }


    const getPatientsRecordsRecovery = ({commit},payload)=> {

    /**
      *  @params payload.data, payload.nextPrev
      *
      *  Set empty string nextPrev if undefined otherwise append the nextPrev to url
      *
      */
      let nextPrev = payload.nextPrev != undefined ? payload.nextPrev : '';
      /**/

    /**
      * Convert JSON to URL query string
      *
      */
      var queryString = Object.keys(payload.data).map((key) => {
        return encodeURIComponent(key) + '=' + encodeURIComponent(payload.data[key])
      }).join('&');
      /**/


      return new Promise((resolve,reject)=> {
        Axios.get(`patients-records-recovery?${queryString} ${nextPrev}`, default_header).then((res)=>{
         if (res.status == 200) {
          commit('setPatientsRecordsRecovery', res.data);
          resolve(true);
        }
      }).catch(err=>{
        reject(err.response.data.errors || err.response.data.message)
      })
    })
    }

    const getPatientInfo = ({commit},payload)=>{
      return new Promise((resolve,reject)=>{
        Axios.get('patients/'+payload, default_header).then((res)=>{
         if (res.status == 200) {
          resolve(res.data);
        }
      }).catch(err=>{
        reject(err.response.data.errors || err.response.data.message)
      })
    })
    }


    const checkExist = ({ commit }, payload) => {
      return new Promise((resolve,reject)=>{
        Axios.post('patients/check-exist', payload, default_header).then((response)=> {
          if(response.status == 200) {
            let res = response.data.response
            return resolve(res);
          }
        }).catch(err=> {
          reject(err.response.data.errors || err.response.data.message)
        })
      })
    }


    export default {
      getPatients,
      createPatient,
      updatePatient,
      deletePatient,
      editPatient,
      getPatientsRecordsRecovery,
      checkExist,
      getPatientInfo
    };
