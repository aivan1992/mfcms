import Axios from "axios";

import {default_header, multipart_header } from '@/request_headers.js'

const getAccounts = ({commit},payload)=> {

    /**
      *  @params payload.data, payload.nextPrev
      *
      *  Set empty string nextPrev if undefined otherwise append the nextPrev to url
      *
      */
      let nextPrev = payload.nextPrev != undefined ? payload.nextPrev : '';
      /**/

    /**
      * Convert JSON to URL query string
      *
      */
      var queryString = Object.keys(payload.data).map((key) => {
        return encodeURIComponent(key) + '=' + encodeURIComponent(payload.data[key])
      }).join('&');
      /**/


      return new Promise((resolve,reject)=> {
        Axios.get(`accounts?${queryString} ${nextPrev}`, default_header).then((res)=>{
         if (res.status == 200) {
          commit('setAccounts', res.data);
          resolve(true);
        }
      }).catch(err =>{
        reject(err.response.data.errors || err.response.data.message)
      })
    })
    }

    const editAccount = ({commit},payload)=>{
      return new Promise((resolve,reject)=>{
        Axios.get(`accounts/${payload.id}/edit` , default_header).then((res)=>{
         if (res.status == 200) {
          commit('setAccount', res.data);
          resolve(true);
        }

      }).catch(err =>{
        reject(err.response.data.errors || err.response.data.message)
      })
    })
    }

    const createAccount = (state, payload) => {
      return new Promise((resolve, reject) => {
        Axios.post("accounts", payload, default_header)
        .then(res => {
          if (res.status == 201) {
            resolve(res.data);
          }
        })
        .catch(err => {
          reject(err.response.data.errors || err.response.data.message);
        });
      });
    };

    const deleteAccount = ({commit},payload)=>{
      return new Promise((resolve,reject)=>{
        Axios.delete('accounts/'+ payload.id, default_header).then((res)=> {
          resolve(res.data)
        }).catch(err =>{
          reject(err.response.data.errors || err.response.data.message)
        })
      })
    };

    const updateAccount = ({commit},payload)=>{
      return new Promise ((resolve,reject)=>{
        Axios.put(`accounts/${payload.table_id}`,payload, default_header).then((res)=>{
          resolve(res.data)
        }).catch(err =>{
          reject(err.response.data.errors || err.response.data.message)
        })
      })
    }

    const changeStatus = ({commit},payload)=>{
      return new Promise ((resolve,reject)=>{
        Axios.post(`accounts/update-status`,payload, default_header)
        .then(res => {
          resolve(true)
        }).catch(err =>{
          reject(err.response.data.errors || err.response.data.message)
        })
      })
    }

    export default {
      getAccounts,
      createAccount,
      updateAccount,
      deleteAccount,
      editAccount,
      changeStatus
    };
