import actions from './actions';
import getters from './getters';
import mutations from './mutations';


const state = {

	accounts: null,
	pagination: null,

	account: {
		permissions: {
			view: false,
			create: false,
			edit: false,
			remove: false
		},
		table_id: '',
		name: '',
		email: '',
		password: '',
		role: ''
	}
	

};

export default {
	namespaced: true,
	state,
	actions,
	getters,
	mutations,
};
