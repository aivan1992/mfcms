
const getAccounts = (state) => {
	return state.accounts;
}

const getPagination = (state) => {
	return state.pagination;
}

const getAccount = (state) => {
	return state.account;
}
export default {
	getAccounts,
	getPagination,
	getAccount
};
