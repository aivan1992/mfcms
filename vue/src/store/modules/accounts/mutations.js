

const setAccounts = (state, payload) => {
	state.accounts = payload.data
	state.pagination = payload.pagination
}

const setAccount = (state, payload) => {
	state.account.table_id = payload.id
	state.account.name = payload.name
	state.account.email = payload.email
	state.account.role = payload.role

	let permissions = payload.permissions;

	state.account.permissions = {
		view: permissions.includes('view'),
		create: permissions.includes('create'),
		edit: permissions.includes('edit'),
		remove: permissions.includes('remove')
	}
}
export default {
	setAccounts,
	setAccount
};
