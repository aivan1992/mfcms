import Axios from "axios";

import {default_header, multipart_header } from '@/request_headers.js'

const getCheckups = ({commit},payload)=> {

    /**
      *  @params payload.data, payload.nextPrev
      *  
      *  Set empty string nextPrev if undefined otherwise append the nextPrev to url 
      *
      */  
      let nextPrev = payload.nextPrev != undefined ? payload.nextPrev : '';
      /**/

    /**
      * Convert JSON to URL query string
      *
      */
      var queryString = Object.keys(payload.data).map((key) => {
        return encodeURIComponent(key) + '=' + encodeURIComponent(payload.data[key])
      }).join('&');
      /**/


      return new Promise((resolve,reject)=> {
        Axios.get(`checkups?${queryString} ${nextPrev}`, default_header).then((res)=>{
         if (res.status == 200) {
          commit('setCheckups', res.data);
          resolve(true);
        }
      }).catch(err=>{
        reject(err.response.data.errors || err.response.data.message)
      })
    })
    }

    const editCheckup = ({commit},payload)=>{
      return new Promise((resolve,reject)=>{
        Axios.get(`checkups/${payload.id}/edit` , default_header).then((res)=>{
         if (res.status == 200) {
          commit('setCheckups', res.data);
          resolve(true);
        }

      }).catch(err=>{
        reject(err.response.data.errors || err.response.data.message)
      })
    })
    }

    // const createCheckup = (state, payload) => {
    //   return new Promise((resolve, reject) => {
    //     Axios.post("checkups", payload, default_header)
    //     .then(res => {
    //       if (res.status == 201) {
    //         resolve(res.data);
    //       }
    //     })
    //     .catch(err => {
    //       reject(err.response.data.errors || err.response.data.message);
    //     });
    //   });
    // };

    const deleteCheckup = ({commit},payload)=>{
      return new Promise((resolve,reject)=>{
        Axios.delete('checkups/'+ payload.id, default_header).then((res)=> {
          resolve(res.data)
        }).catch(err=>{
          reject(err.response.data.errors || err.response.data.message)
        })
      })
    }

    const updateCheckup = ({commit},payload)=>{
      return new Promise ((resolve,reject)=>{
        Axios.put(`checkups/${payload.table_id}`,payload, default_header).then((res)=>{
          resolve(res.data)
        }).catch(err=>{
          reject(err.response.data.errors || err.response.data.message)
        })
      })
    }




    export default {
      getCheckups,
      // createCheckup,
      updateCheckup,
      deleteCheckup,
      editCheckup
    };
