import Axios from "axios";

import {default_header, multipart_header } from '@/request_headers.js'



const getPatientCheckupRecords = ({commit},payload)=> {

    /**
      *  @params payload.data, payload.nextPrev
      *  
      *  Set empty string nextPrev if undefined otherwise append the nextPrev to url 
      *
      */  
      let nextPrev = payload.nextPrev != undefined ? payload.nextPrev : '';
      /**/

    /**
      * Convert JSON to URL query string
      *
      */
      var queryString = Object.keys(payload.data).map((key) => {
        return encodeURIComponent(key) + '=' + encodeURIComponent(payload.data[key])
      }).join('&');
      /**/


      return new Promise((resolve,reject)=> {
        Axios.get(`checkups/patient-checkup-records?${queryString} ${nextPrev}`, default_header).then((res)=>{
         if (res.status == 200) {
          commit('setPatientCheckupRecords', res.data);
          resolve(true);
        }
      }).catch(err=>{
        reject(err.response.data.errors || err.response.data.message)
      })
    })
    }


    const createCheckup = (state, payload) => {
      return new Promise((resolve, reject) => {
        Axios.post("checkups/patient-checkup-records", payload, default_header)
        .then(res => {
          if (res.status == 201) {
            resolve(res.data);
          }
        })
        .catch(err => {
          reject(err.response.data.errors || err.response.data.message);
        });
      });
    };


    const showCheckups = ({commit,state}, payload) => {
      return new Promise((resolve, reject) => {
        Axios.get(`checkups/patient-checkup-records/${payload.id}`, default_header)
        .then(res => {
          if (res.status == 200) {
           commit('setShowCheckups', res.data);
           resolve(true);
         }
       })
        .catch(err => {
          reject(err.response.data.errors || err.response.data.message);
        });
      });
    };


    export default {
      getPatientCheckupRecords,
      createCheckup,
      showCheckups
    };
