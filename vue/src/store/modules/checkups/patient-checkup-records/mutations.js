const setPatientCheckupRecords = (state, payload) => {
	state.patient_checkup_records = payload.data
	state.pagination = payload.pagination
}

const setShowCheckups = (state, payload) => {
	state.show_checkups = payload
}

export default {
	setPatientCheckupRecords,
	setShowCheckups
};
