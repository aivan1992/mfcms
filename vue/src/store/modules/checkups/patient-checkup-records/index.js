import actions from './actions';
import getters from './getters';
import mutations from './mutations';


const state = {

	patient_checkup_records: null,
	pagination: null,

	show_checkups: null

};

export default {
	namespaced: true,
	state,
	actions,
	getters,
	mutations,
};
