const getPatientCheckupRecords = (state) => {
	return state.patient_checkup_records;
}

const getPagination = (state) => {
	return state.pagination;
}


const getShowCheckups = (state) => {
	return state.show_checkups;
}

export default {
	getPagination,
	getPatientCheckupRecords,
	getShowCheckups
};
