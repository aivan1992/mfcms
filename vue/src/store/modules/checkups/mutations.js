

const setCheckups = (state, payload) => {
	state.checkups = payload.data
	state.pagination = payload.pagination
}

export default {
	setCheckups
};
