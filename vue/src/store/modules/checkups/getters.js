
const getCheckups = (state) => {
	return state.checkups;
}

const getPagination = (state) => {
	return state.pagination;
}



export default {
	getCheckups,
	getPagination,
};
