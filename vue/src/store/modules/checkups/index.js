import actions from './actions';
import getters from './getters';
import mutations from './mutations';


const state = {
	checkups: null,
	pagination: null,
};

import patient_checkup_records from './patient-checkup-records'

export default {
	namespaced: true,
	modules: {
		patient_checkup_records
	},
	state,
	actions,
	getters,
	mutations,
};
