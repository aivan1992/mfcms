import Axios from "axios";

Axios.interceptors.response.use(
  res => {
    return res;
  },
  err => {
    if (err.response.status == 401) {
      try {
        if (localStorage.getItem("token")) throw 401;
      } catch {
        window.localStorage.clear();
        location.href = "/";
      }
    }
    return Promise.reject(err);
  }
);
