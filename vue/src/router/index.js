import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

const DefaultContainer = () => import ("@/views/templates/DefaultContainer.vue")

export default new Router({
  mode: "history",
  // base: process.env.BASE_URL,
  routes: [{
    path: "/",
    redirect: '/dashboard',
    name: "DefaultContainer",
    component: DefaultContainer,
    children: [
    {
      path: "dashboard",
      name: "Dashboard",
      component: () => import ("@/views/Dashboard.vue"),
      meta: {
        title: `Dashboard - Mom's Family Clinic`,
      }
    },

    {
      path: "accounts",
      name: "Accounts",
      component: () => import ("@/views/pages/accounts/Index.vue"),
      meta: {
        title: `Accounts - Mom's Family Clinic`,
      }
    },


    {
      path: "patients",
      name: "Patients",
      component: () => import ("@/views/pages/patients/Index.vue"),
      meta: {
        title: `Patients - Mom's Family Clinic`,
      }
    },


    {
      path: "checkups",
      name: "Checkups",
      component: () => import ("@/views/pages/checkups/Index.vue"),
      meta: {
        title: `Checkups - Mom's Family Clinic`,
      }
    }

    ]

  }, 




  {
    path: '/accounts',
    component: DefaultContainer,
    children: [

    {
      path: "create",
      name: "Create Account",
      component: () => import ("@/views/pages/accounts/Create.vue"),
      meta: {
        title: `Accounts - Create - Mom's Family Clinic`,
      }
    },


    {
      path: "edit/:id",
      name: "Edit Account",
      component: () => import ("@/views/pages/accounts/Edit.vue"),
      meta: {
        title: `Accounts - Edit - Mom's Family Clinic`,
      }
    },

    ]
  },



  {
    path: '/checkups',
    component: DefaultContainer,
    children: [

    {
      path: "create",
      name: "Create Checkup",
      component: () => import ("@/views/pages/checkups/Create.vue"),
      meta: {
        title: `Checkups - Create - Mom's Family Clinic`,
      }
    },


    {
      path: "edit/:id",
      name: "Edit Checkup",
      component: () => import ("@/views/pages/checkups/Edit.vue"),
      meta: {
        title: `Checkups - Edit - Mom's Family Clinic`,
      }
    },

    ]
  },






  {
    path: '/checkups',
    component: DefaultContainer,
    children: [

    {
      path: "patient-checkup-records",
      name: "Patient Checkup Records",
      component: () => import ("@/views/pages/checkups/patient-checkup-records/Index.vue"),
      meta: {
        title: `Patient checkup records - Mom's Family Clinic`,
      }
    },

    ]
  },




  {
    path: '/checkups/patient-checkup-records',
    component: DefaultContainer,
    children: [

    {
      path: "create/:name/:id",
      name: "Create Patient Checkup Records",
      component: () => import ("@/views/pages/checkups/patient-checkup-records/Create.vue"),
      meta: {
        title: `Patient checkup records - Create - Mom's Family Clinic`,
      }
    },

     {
      path: "show/:name/:id",
      name: "Show Patient Checkup Records",
      component: () => import ("@/views/pages/checkups/patient-checkup-records/Show.vue"),
      meta: {
        title: `Patient checkup records - Show - Mom's Family Clinic`,
      }
    },


 {
      path: "edit/:name/:id",
      name: "Edit Patient Checkup Records",
      component: () => import ("@/views/pages/checkups/patient-checkup-records/Edit.vue"),
      meta: {
        title: `Patient checkup records - Edit - Mom's Family Clinic`,
      }
    },


    ]
  },





  {
    path: '/patients',
    component: DefaultContainer,
    children: [

    {
      path: "create",
      name: "Create Patients",
      component: () => import ("@/views/pages/patients/Create.vue"),
      meta: {
        title: `Patients - Create - Mom's Family Clinic`,
      }
    },

    {
      path: "edit/:id",
      name: "Edit Patient",
      component: () => import ("@/views/pages/patients/Edit.vue"),
      meta: {
        title: `Patient - Edit - Mom's Family Clinic`,
      }
    },

    {
      path: "recovery",
      name: "Patients Record Recovery",
      component: () => import ("@/views/pages/patients/Recovery.vue"),
      meta: {
        title: `Patienty - Record Recovery - Mom's Family Clinic`,
      }
    },

    ]
  },


  {
    path: '/',
    component: {
      render (c) { return c('router-view') }
    },
    children: [

    {
      path: "/auth",
      name: "Auth",
      component: () =>  import ("@/views/pages/auth/index.vue"),
      meta: {
        noAuth: true,
        title: `Auth - Mom's Family Clinic`,
      }
    }, {
      path: "*",
      redirect: '/dashboard',
    }
    ],
  }
  ]
});