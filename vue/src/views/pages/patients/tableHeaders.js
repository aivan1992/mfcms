export default {
	columns: [
		{
			field: "id",
			label: "ID",
			tdClass: "p-4",
		},
		{
			field: "name",
			label: "NAME",
			tdClass: "text-nowrap",
		},
		{
			field: "date_of_birth",
			label: "DATE OF BIRTH",
			thClass: "text-nowrap",
		},
		{
			field: "sex",
			label: "SEX",
		},
		{
			field: "civil_status",
			label: "CIVIL STATUS",
		},
		{
			field: "add_remove",
			// label: 'ADD CHECKUP / REMOVE PATIENT',
			label: "ADD CHECKUP",
			thClass: "text-nowrap",
		},
	],
};
