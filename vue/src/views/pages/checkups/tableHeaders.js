// 'id' => $this->id,
// 'patient_id' => $this->patient_id,
// 'date_of_check_up' => $this->date_of_check_up
// 'treatment' => $this->treatment,
// 'findings' => $this->findings,
// 'complaints' => $this->complaints,
// 'weight' => $this->weight,
// 'temperature' => $this->temperature,
// 'height' => $this->height,
// 'patient_name' => $this->patientName

export default {
	columns: [
	{
		field: 'id',
		label: 'CHECKUP ID',
		tdClass: 'p-4',
		thClass: 'text-nowrap'

	}, {
		field: 'patient_id',
		label: 'PATIENT ID',
		thClass: 'text-nowrap'
	}, {
		field: 'patient_name',
		label: 'PATIENT NAME',
		thClass: 'text-nowrap'
	}, {
		field: 'date_of_check_up',
		label: 'CHECKUP DATE',
		tdClass: 'text-nowrap',
		thClass: 'text-nowrap'
	}, {
		field: 'complaints',
		label: 'COMPLAINTS',
	},{
		field: 'findings',
		label: 'FINDINGS',
	}, {
		field: 'treatment',
		label: 'TREATMENT',
	 },
	  // {
	// 	field: 'remove',
	// 	label: 'REMOVE RECORD',
	// 	thClass: 'text-nowrap'
	// }
	],
	
}