export default {
	columns: [
	{
		field: 'id',
		label: 'PATIENT ID',
		tdClass: 'p-4'

	}, {
		field: 'name',
		label: 'PATIENT NAME',
		tdClass: 'text-nowrap'
	}, {
		field: 'date_of_birth',
		label: 'DATE OF BIRTH',
			thClass: 'text-nowrap'
	}, {
		field: 'sex',
		label: 'SEX',
	}, {
		field: 'civil_status',
		label: 'CIVIL STATUS',
	}, {
		field: 'show_edit_add',
		label: 'SHOW / ADD CHECKUP',
			thClass: 'text-nowrap'
	}
	],
	
}