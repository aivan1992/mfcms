export default {
	columns: [
	{
		field: 'id',
		label: 'ID',
		tdClass: 'p-4'

	}, {
		field: 'name',
		label: 'NAME',
		tdClass: 'text-nowrap'
	}, {
		field: 'email',
		label: 'EMAIL',
		tdClass: 'text-nowrap'
	}, {
		field: 'role',
		label: 'ROLE',
		thClass: 'text-nowrap'
	},{
		field: 'permissions',
		label: 'PERMISSIONS',
	}, {
		field: 'status',
		label: 'STATUS',
	}, {
		field: 'deac_suspend_remove',
		label: 'UNSUSPEND / SUSPEND',
			thClass: 'text-nowrap'
	}
	],
	
}